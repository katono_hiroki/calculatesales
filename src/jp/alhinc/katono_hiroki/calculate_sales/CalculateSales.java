package jp.alhinc.katono_hiroki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CalculateSales {

	public static void main(String[] args) {
		
		HashMap<String , String> branchNames = new HashMap<>();
		HashMap<String , Long> branchSales = new HashMap<>();
		
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		
		if(!input(args[0] , "branch.lst", branchNames , branchSales)) {
			return;
		}
		
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		List<Integer> num = new ArrayList<>();
		
		for(int i = 0; i < files.length ; i++) {
			String fileName = files[i].getName();
			if(fileName.matches("^[0-9]{8}.rcd$") && files[i].isFile()) {
				rcdFiles.add(files[i]);
				
				String[] name = files[i].getName().split("\\.");
				num.add(Integer.parseInt(name[0]));
			}
		}
		
		for(int i = 0; i < num.size(); i++){
			if(num.get(i) != i + 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				
				String line;
				List<String> rcdLineList = new ArrayList<>();
				
				while((line = br.readLine()) != null) {
					rcdLineList.add(line);
				}
				
				String code = rcdLineList.get(0);
				if(!branchSales.containsKey(code)) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}
				String sales = rcdLineList.get(1);
				
				if(rcdLineList.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
				if(!sales.matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long amount = Long.parseLong(sales);
				
				if(branchSales.containsKey(code)) {
					branchSales.put(code , branchSales.get(code) + amount);
					
					long totalamount = branchSales.get(code) + amount;
					int digit = Long.toString(totalamount).length();
					if(digit > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}
				
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br !=null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		
		if(!output(args[0] , "branch.out", branchNames , branchSales)) {
			return;
		}
	}
	
	public static boolean input(String filepath , String fileName ,
			HashMap<String , String> branchNames , HashMap<String , Long> branchSales) {
		BufferedReader br = null;
		try {
			File infile = new File(filepath, fileName);
			if(!infile.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(infile);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				if(!line.contains(",")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				String[] items = line.split("," , -1);
				if(items.length != 2 || !items[0].matches("^[0-9]{3}$")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
					}
				branchNames.put(items[0] , items[1]);
				branchSales.put(items[0] , 0L);
			}
			
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br !=null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	
	public static boolean output(String filepath , String fileName ,
			HashMap<String , String> branchNames , HashMap<String , Long> branchSales) {
		BufferedWriter bw = null;
		try {
			File outfile = new File(filepath, fileName);
			FileWriter fw = new FileWriter(outfile);
			bw = new BufferedWriter(fw);
			
			for(String key : branchNames.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			try {
				if(bw != null) {
					bw.close();
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
				return false;
			}
		}
		return true;
	}
	
}
